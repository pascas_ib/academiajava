package com.academia;

import org.junit.Assert;
import org.junit.Test;

public class OperacionesAritmeticasTest {

	@Test
    public void sumaTest() {
        Assert.assertTrue(OperacionesAritmeticas.suma(1, 2) == 3);
    }

}
